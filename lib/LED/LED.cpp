#include "LED.h"

LED::LED() {
    pinMode(WIFI_LED_PIN, OUTPUT);
    pinMode(COMM_LED_PIN, OUTPUT);
}

void LED::onWIFI(bool flag) {
    digitalWrite(WIFI_LED_PIN, flag);
}
void LED::onCOMM(bool flag) {
    digitalWrite(COMM_LED_PIN, flag);
}