#ifndef LED_h
#define LED_h

#include <Arduino.h>

#define WIFI_LED_PIN 33
#define COMM_LED_PIN 19

class LED {
    public:
        LED();
        void onWIFI(bool flag);
        void onCOMM(bool flag);

    private:

};

#endif