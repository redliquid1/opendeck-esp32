#include "Buttons.h"

Buttons * Buttons::self = nullptr;
Ticker * Buttons::buttonsTicker = nullptr;
Buttons::Buttons() {
    self = this;
    Wire.begin(21, 22, 100000);

    pinMode(PCF_INTERRUPT, INPUT);
    initInterrupts();
    initTickers();
}

IRAM_ATTR void Buttons::initInterrupts() {
    attachInterrupt(digitalPinToInterrupt(PCF_INTERRUPT), &Buttons::handleInterrupt, CHANGE);
}

IRAM_ATTR void Buttons::detachInterrupts() {
    detachInterrupt(digitalPinToInterrupt(PCF_INTERRUPT));
}

IRAM_ATTR void Buttons::handleInterrupt() {
    Buttons::self->shouldHandlePressFlag = true;
}

bool Buttons::shouldHandlePress() {
    return this->shouldHandlePressFlag;
}

void Buttons::handlePress() {
    this->buttons = this->checkButtons();
    if (self->buttons == 0xffff && self->previousButtons != 0xffff) {
        self->doAction();
        self->previousButtons = 0xffff;
    }    
    else if (self->buttons != 0xffff and self->previousButtons == 0xffff) {
        self->previousButtons = self->buttons;
        self->buttons = 0xffff;
    }
    this->shouldHandlePressFlag = false;
}

void Buttons::doAction() {
    uint16_t s = ~this->previousButtons;
    for (uint8_t i = 0; i < 16; i++) {
        uint16_t test = 1 << i;
        if (s & test) {
            this->buttonPressHandler(i);
            break;
        }
    }
}

void Buttons::initTickers() {
    buttonsTicker = new Ticker([&](){
        if (self->getButtonsState() == 0xffff) { // all buttons up.
            self->timerCounter = 0;
        }
        else {
            if (self->timerCounter > TIMER_REPEAT_FIRST_PAUSE) {
                self->doAction();
            }
            else {
                self->timerCounter++;
            }
            
        }
    }, TIMER_REPEAT, MILLIS);
}

void Buttons::updateTickers() {
    this->buttonsTicker->update();
}

uint16_t Buttons::getButtonsState() {
    return this->buttons;
}

uint16_t Buttons::checkButtons() {
    uint8_t first = 0xff;
    uint8_t second = 0xff;

    Wire.requestFrom(PCF_ADDRESS_1, 1);
    first = (char)Wire.read();
    if (Wire.available()) {
        first = (char)Wire.read();
    }

    Wire.requestFrom(PCF_ADDRESS_2, 1);
    if (Wire.available()) {
        second = (char)Wire.read();
    }
    
    return (uint16_t(second) << 8) | first;
}

void Buttons::setButtonPressHandler(std::function<void(uint8_t)> handler) {
    this->buttonPressHandler = handler;
}