#ifndef Buttons_h
#define Buttons_h

#include <Arduino.h>
#include <Wire.h>
#include <Ticker.h>

#define PCF_ADDRESS_1 0x20
#define PCF_ADDRESS_2 0x21
#define PCF_INTERRUPT 32
#define TIMER_REPEAT 100
#define TIMER_REPEAT_FIRST_PAUSE 5 

class Buttons {
    public:
        Buttons();
        uint16_t getButtonsState();
        void updateTickers();
        bool shouldHandlePress();
        void handlePress();
        void doAction();
        void setButtonPressHandler(std::function<void(uint8_t)> handler);

        uint32_t timerCounter = 0;
        static Ticker * buttonsTicker;
    private:
        static Buttons * self;

        std::function<void(uint8_t)> buttonPressHandler;

        uint16_t checkButtons();
        void initTickers();
        uint16_t buttons = 0xffff;
        uint16_t previousButtons = 0xffff;
        bool shouldHandlePressFlag = false;

        static void handleInterrupt();
        static void initInterrupts();
        static void detachInterrupts();        
};

#endif