#include "LCD.h"

#include <stdio.h>
#include <string.h>
#include <inttypes.h>
#include "Arduino.h"
#include <Wire.h>

LCD::LCD() {
    pinMode(SHIFT_DATA_PIN, OUTPUT);
    pinMode(SHIFT_LATCH_PIN, OUTPUT);
    pinMode(SHIFT_CLOCK_PIN, OUTPUT);

    tft = new TFT_eSPI();
    tft->init();

    
}

void LCD::selectLCD(uint8_t number) {
    selectedLCD = number;
    uint32_t bits = uint16_t(~(1 << selectedLCD));

    bits |= backlightEnabledForLCDs;

    this->outBits(bits);
}

void LCD::setBacklight(uint8_t number, bool enabled) {
    if (enabled) {
        backlightEnabledForLCDs |= 1 << number;
    }
    else {
        backlightEnabledForLCDs &= ~(1 << number);
    }

    uint32_t bits = uint16_t(~(1 << selectedLCD));;
    bits |= backlightEnabledForLCDs << 16;

    this->outBits(bits);
}

void LCD::outBits(uint32_t bits) {
    digitalWrite(SHIFT_LATCH_PIN, LOW);
    this->shift(SHIFT_DATA_PIN, SHIFT_CLOCK_PIN, MSBFIRST, bits);
    digitalWrite(SHIFT_LATCH_PIN, HIGH);

    Serial.println(String(bits, BIN));
}

void LCD::shift(int8_t dataPin, uint8_t clockPin, uint8_t bitOrder, uint32_t val) {
    uint8_t i;

    for (i = 0; i < 32; i++) { 
        if(bitOrder == LSBFIRST) {
            digitalWrite(dataPin, !!(val & (1 << i)));
        }
        else {
            digitalWrite(dataPin, !!(val & (1 << (31 - i))));
        }

        digitalWrite(clockPin, HIGH);
        // delayMicroseconds(100);
        digitalWrite(clockPin, LOW);
    }
}

void LCD::waitUntilComplete() {
    yield();
    delay(200);
    
}

void LCD::testLCD() {

    this->selectLCD(0);
    this->setBacklight(0, true);

    tft->fillScreen(TFT_RED);
  
    // Set "cursor" at top left corner of display (0,0) and select font 4
    tft->setCursor(0, 0, 1);

    // Set the font colour to be white with a black background
    tft->setTextColor(TFT_WHITE, TFT_BLACK);

    // We can now plot text on screen using the "print" class
    tft->println("Sample text 1\n");
    tft->println("Sample text 2"); 


    this->selectLCD(1);
    this->setBacklight(1, true);
    
    tft->fillScreen(TFT_BLUE);
  
    // Set "cursor" at top left corner of display (0,0) and select font 4
    tft->setCursor(0, 0, 1);

    // Set the font colour to be white with a black background
    tft->setTextColor(TFT_RED, TFT_BLACK);

    // We can now plot text on screen using the "print" class
    tft->println("XZXCVdsfgh\n");
    tft->println("sdfgksadnfguj"); 


    Serial.println("LCD written.");
}