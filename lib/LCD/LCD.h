#ifndef LCD_h
#define LCD_h

#include <Arduino.h>
#include <SPI.h>
#include <TFT_eSPI.h>

using namespace std; 

#define SHIFT_DATA_PIN  25
#define SHIFT_LATCH_PIN 26
#define SHIFT_CLOCK_PIN 27

#define PCF_INTERRUPT_PIN 32

class LCD {
    public:
        LCD();

        void testLCD();
        void selectLCD(uint8_t number);
        void setBacklight(uint8_t number, bool enabled);
        void waitUntilComplete();

    protected:
        void shift(int8_t dataPin, uint8_t clockPin, uint8_t bitOrder, uint32_t val);
        void outBits(uint32_t bits);

        TFT_eSPI * tft;

        uint8_t selectedLCD = 0;
        uint16_t backlightEnabledForLCDs = 0;
};

#endif