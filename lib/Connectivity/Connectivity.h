#ifndef Connectivity_h
#define Connectivity_h

#include <Arduino.h>
#include <LED.h>
#include <Buttons.h>
#include <LCD.h>
#include <WiFi.h>
#include <ESPAsyncWebServer.h>
#include <ESPAsyncWiFiManager.h> 
#include "WebSockets/WebSockets.h"

class Connectivity {
    public:
        Connectivity(LED * led, Buttons * buttons, LCD * lcd);
        AsyncWiFiManager * getWifiManager();

    private:
        void prepareAP();
        void connectWIFI();
        void prepareStaticPages();

        String apName;
        WebSockets * webSockets;
        LED * led;
        Buttons * buttons;
        LCD * lcd;
        AsyncWebServer * server;
        DNSServer * dns;
        AsyncWiFiManager * wifiManager;

};

#endif