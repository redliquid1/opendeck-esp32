#include "Connectivity.h"

Connectivity::Connectivity(LED * led, Buttons * buttons, LCD * lcd) {
    this->led = led;
    this->buttons = buttons;
    this->lcd = lcd;

    this->server = new AsyncWebServer(80);
    this->dns = new DNSServer();
    this->wifiManager = new AsyncWiFiManager(this->server, this->dns);

    this->prepareAP();
    this->connectWIFI();
    this->prepareStaticPages();

    // prepare websockets
    this->webSockets = new WebSockets();
    this->webSockets->attachButtonsHandler(this->buttons);
    this->server->addHandler(this->webSockets->getServer());

    

    this->server->begin();
}

void Connectivity::prepareAP() {
    byte mac[6];
    WiFi.macAddress(mac);
    this->apName = "OpenDeck-" + String(mac[0], HEX) + String(mac[1], HEX) +String(mac[2], HEX) +String(mac[3], HEX) + String(mac[4], HEX) + String(mac[5], HEX);

    WiFi.config(INADDR_NONE, INADDR_NONE, INADDR_NONE, INADDR_NONE);
    WiFi.setHostname(this->apName.c_str());
}

void Connectivity::connectWIFI() {
    this->wifiManager->autoConnect(this->apName.c_str());    
    this->led->onWIFI(true);
}

void Connectivity::prepareStaticPages() {
    this->server->on("/heap", HTTP_GET, [](AsyncWebServerRequest * request){
        request->send(200, "text/plain", (String)ESP.getFreeHeap());
    });
}


AsyncWiFiManager * Connectivity::getWifiManager() {
    return this->wifiManager;
}

