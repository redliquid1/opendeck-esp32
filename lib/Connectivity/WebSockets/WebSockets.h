#ifndef WebSockets_h
#define WebSockets_h

#include <Arduino.h>
#include <ESPAsyncWebServer.h>
#include "SocketData.h"
#include "Out/ButtonPush.h"
#include <Buttons.h>

#define WEBSOCKET_URL "/ws"

class WebSockets {
    public:
        WebSockets();
        AsyncWebSocket * getServer();
        void attachButtonsHandler(Buttons * buttons);

    private:
        AsyncWebSocket * ws;
        Buttons * buttons;
        static AsyncWebSocketClient * client;
        static SocketData * socketData;

};

#endif