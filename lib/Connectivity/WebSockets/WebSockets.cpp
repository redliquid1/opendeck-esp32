#include "WebSockets.h"

SocketData * WebSockets::socketData = nullptr;
AsyncWebSocketClient * WebSockets::client = nullptr;
WebSockets::WebSockets() {
    WebSockets::socketData = new SocketData();

    this->ws = new AsyncWebSocket(WEBSOCKET_URL);
    this->ws->onEvent([](AsyncWebSocket * server, AsyncWebSocketClient * client, AwsEventType type, void * arg, uint8_t * data, size_t len) {
        if (type == WS_EVT_CONNECT) {
            // only the first client will be valid.
            if (WebSockets::client == nullptr) {
                WebSockets::client = client;
            }
            client->text("x:OpenDeck online.");
        }
        else if (type == WS_EVT_DISCONNECT) {
            WebSockets::client = nullptr;
        }
        else if (type == WS_EVT_DATA) {
            WebSockets::socketData->processIncomeData(data, len);
        }
    });
}

void WebSockets::attachButtonsHandler(Buttons * buttons) {
    this->buttons = buttons;
    this->buttons->setButtonPressHandler([&](uint8_t button) {
        if (WebSockets::client != nullptr) {
            WebSockets::client->text(ButtonPush::getResponse(button));
        }
    });
}

AsyncWebSocket * WebSockets::getServer() {
    return this->ws;
}