#ifndef SocketData_h
#define SocketData_h

#include <Arduino.h>

class SocketData {
    public:
        SocketData();

        void processIncomeData(uint8_t * data, size_t len);
};

#endif