#ifndef ButtonPush_h
#define ButtonPush_h

#include <Arduino.h>

class ButtonPush {
    public:
        ButtonPush();

        static String getResponse(uint8_t number);
};

#endif