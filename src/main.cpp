#include <FS.h>

#include <Arduino.h>
#include "LCD.h"
#include "Buttons.h"
#include "Connectivity.h"
#include "LED.h"

#include <WiFi.h>

Buttons * buttons;
Connectivity * connectivity;
LED * led;
LCD * lcd;

void setup() {
    Serial.begin(115200);
    delay(10);
    Serial.println("ESP32 init complete.");

    led = new LED();
    buttons = new Buttons();
    lcd = new LCD();
    connectivity = new Connectivity(led, buttons, lcd);

    Serial.println("Wifi connected");
}

void loop() {
    buttons->updateTickers();

    if (buttons->shouldHandlePress()) {
        buttons->handlePress();
    }
}